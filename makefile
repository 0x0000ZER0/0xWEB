FILES=main.c z0_web_serv.c z0_web_req.c 0xSTRING/z0_string.c 0xLIST/z0_list.c 0xTHPOOL/z0_thpool.c
MODULES=-I0xSTRING -I0xLIST -I0xTHPOOL

main:
	gcc $(FILES) $(MODULES) -O2 -pedantic -Wall -o 0xWEB -march=native -lpthread

debug:
	gcc $(FILES) $(MODULES) -O2 -pedantic -Wall -o 0xWEB -march=native -lpthread -DZ0_DEBUG

clear:
	rm ./0xWEB
