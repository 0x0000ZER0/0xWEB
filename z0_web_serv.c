#include "z0_web_decl.h"

#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>

typedef struct sockaddr	   sockaddr;
typedef struct sockaddr_in sockaddr_in;

typedef struct {
	z0_web_server *server;
	int		 conn;
} z0_arg;

static volatile bool _running_;
static void
signal_handler(int data) {
	_running_ = false;
}

static void*
z0_web_server_handle(void *arg) {
	z0_arg *z_arg;
	z_arg = (z0_arg*)arg;

	z0_web_server *server;
	server = z_arg->server;
	
	int conn;
	conn = z_arg->conn;

	z0_string buff;
	buff = z0_string_init_len(server->buff_len);

	ssize_t bytes;
	bytes = recv(conn, buff, server->buff_len, 0);
	if (bytes == -1) {
		perror("ERROR: could not receive the data.");
		z0_string_free(buff);

		shutdown(conn, SHUT_RDWR);
		close(conn);
		return NULL;
	}

	z0_req request;
	z0_web_request_parse(&request, buff);

	size_t i;
	i = 0;

	bool found;
	found = false;
	for (; i < server->paths.len; ++i) {
		void *path;
		path = (uint8_t*)server->paths.data + i * sizeof (z0_string);

		void *method;
		method = (uint8_t*)server->methods.data + i * sizeof (uint8_t);

		found = z0_string_cmp(request.path, *(z0_string*)path) &
			z0_string_cmp(request.method, *(z0_string*)method);
		
		if (found == true)
			break;
	}		
			
	if (found == true) {
		void *ptr;
		ptr = (uint8_t*)server->funcs.data + i * sizeof (z0_web_func);	

		z0_web_func func;
		func = *(z0_web_func*)ptr;

		z0_resp response;			

		func(&request, &response); 

		size_t s_len;
		s_len = z0_string_len(response.status);

		size_t b_len;
		b_len = z0_string_len(response.body);

		size_t e_len; // extra length for "HTTP/1.1" and "\n\n"
		e_len = 10;

		size_t data_len;
		data_len = e_len + b_len + s_len;	

		z0_string data;			
		data = z0_string_init_len(data_len);

		strcat(data, "HTTP/1.1");
		strcat(data + 8, response.status);
		strcat(data + 8 + s_len, "\n\n");
		strcat(data + 8 + s_len + 2, response.body);
		
		bytes = send(conn, data, data_len, 0);
		if (bytes == -1)
			perror("ERROR: could not send the data.");

		z0_string_free(data);	
		z0_string_free(response.status);
		z0_string_free(response.body);
	} else {
		char text[] = "HTTP/1.1 404 Not Found\n\n<h1>Not Found.</hi>";
		bytes = send(conn, text, sizeof (text), 0);
		if (bytes == -1)
			perror("ERROR: could not send the data.");
	}

	z0_web_request_free(&request);
	z0_string_free(buff);


	shutdown(conn, SHUT_RDWR);
	close(conn);
	return NULL;
}

void
z0_web_server_init(z0_web_server *server) {
	server->sock = socket(AF_INET, SOCK_STREAM, 0);

	int option;
	setsockopt(server->sock, SOL_SOCKET, SO_REUSEADDR, &option, sizeof (option));

	if (server->sock == -1) {
		perror("ERROR: could not create the socket.");
		exit(-1);
	}

	sockaddr_in serv_addr;
	memset(&serv_addr, 0, sizeof (serv_addr));

	serv_addr.sin_family	  = AF_INET;
	serv_addr.sin_port	  = htons(server->port);
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);

	int res;
	res = bind(server->sock, (sockaddr*)&serv_addr, sizeof (serv_addr));
	if (res == -1) {
		perror("ERROR: not able to bind.");
		exit(-1);
	}

	res = listen(server->sock, SOMAXCONN);
	if (res == -1) {
		perror("ERROR: not able to listen.");
		close(server->sock);
		exit(-1);
	}

	_running_ = true;

	signal(SIGINT, signal_handler);

	z0_thpool_init(&server->thpool, server->max_conn, server->max_thds);	

	z0_list_init(&server->paths, sizeof (z0_string));
	z0_list_init(&server->methods, sizeof (z0_string));
	z0_list_init(&server->funcs, sizeof (z0_web_func));
}

void
z0_web_server_register(z0_web_server *server, 
			 char   	 *path,
			 char	 	 *method,
			 z0_web_func 	  func) {
	z0_string s_path;
	s_path = z0_string_init(path);
	
	z0_string s_method;
	s_method = z0_string_init(method);

#ifdef Z0_DEBUG
	printf("Z0 WEB DEBUG: REGISTERED [%s] AS [%s] METHOD.\n", s_path, s_method);
#endif

	z0_list_add(&server->paths, (void*)&s_path, sizeof (s_path));
	z0_list_add(&server->methods, (void*)&s_method, sizeof (s_method));
	z0_list_add(&server->funcs, (void*)&func, sizeof (func));
} 

void
z0_web_server_run(z0_web_server *server) {
	do {
		sockaddr_in cli_addr;
		socklen_t   cli_len;
		int 	    conn;	
		conn = accept(server->sock, (sockaddr*)&cli_addr, &cli_len);
		if (conn == -1) {
			perror("ERROR: could not accept the client.");
			continue;
		}
		
		z0_arg arg;
		arg.server = server;
		arg.conn   = conn;

		z0_task task;
		task.func = z0_web_server_handle;
		task.arg  = (void*)&arg;

		z0_thpool_push(&server->thpool, &task);
	} while (_running_);

	//z0_thpool_wait(&server->thpool);
}

void
z0_web_server_free(z0_web_server *server) {
	z0_list_free(&server->funcs);
	
	for (size_t i = 0; i < server->methods.len; ++i) {
		void *ptr;
		ptr = (uint8_t*)server->methods.data + i * sizeof (z0_string);

		z0_string_free(*(z0_string*)ptr);
	}	
	z0_list_free(&server->methods);
	
	for (size_t i = 0; i < server->paths.len; ++i) {
		void *ptr;
		ptr = (uint8_t*)server->paths.data + i * sizeof (z0_string);

		z0_string_free(*(z0_string*)ptr);
	}	
	z0_list_free(&server->paths);
	z0_thpool_free(&server->thpool);
	
	shutdown(server->sock, SHUT_RDWR);
	close(server->sock);
}

























