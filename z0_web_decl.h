#ifndef Z0_WEB_SERVER_H
#define Z0_WEB_SERVER_H

#include <stdint.h>
#include <stddef.h>

#include "z0_string.h"
#include "z0_list.h"
#include "z0_thpool.h"

#define Z0_HTTP_GET    "GET"
#define Z0_HTTP_POST   "POST"
#define Z0_HTTP_PUT    "PUT"
#define Z0_HTTP_DELETE "DELETE"

#define Z0_STATUS_OK    	"200 OK"
#define Z0_STATUS_REDIRECT	"302 Redirect"
#define Z0_STATUS_NOT_FOUND	"404 Not Found"
#define Z0_STATUS_ERROR 	"500 Error"

typedef struct {
	z0_string key;
	z0_string val;
} z0_hdr;

typedef struct {
	z0_string method;
	z0_string path;	
	z0_list   headers;
} z0_req;

typedef struct {
	z0_string status;
	z0_string body;
} z0_resp;

typedef struct {
	size_t      buff_len;
	uint16_t    port;
	int 	    sock;
	uint32_t    max_conn;
	uint32_t    max_thds;
	z0_thpool thpool;
	z0_list   paths;
	z0_list   methods;
	z0_list   funcs;
} z0_web_server;

typedef void (*z0_web_func)(z0_req*, z0_resp*);

void
z0_web_server_init(z0_web_server*);

void
z0_web_server_register(z0_web_server*, char*, char*, z0_web_func); 

void
z0_web_server_run(z0_web_server*);

void
z0_web_server_free(z0_web_server*);

void
z0_web_request_parse(z0_req*, z0_string);

void
z0_web_request_free(z0_req*);


#endif
