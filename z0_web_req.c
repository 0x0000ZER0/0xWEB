#include "z0_web_decl.h"


#ifdef Z0_DEBUG
#include <stdio.h>
#endif

#include <stdint.h>
#include <stdbool.h>

void
z0_web_request_parse(z0_req *request, z0_string buff) {
	size_t i;

	i = 0;
	while (buff[i] != ' ')
		++i;
	buff[i] = '\0';
	request->method = z0_string_init(buff);	
	buff += i + 1;

	i = 0;
	while (buff[i] != ' ')
		++i;
	buff[i] = '\0';
	request->path = z0_string_init(buff);
	buff += i + 1;

#ifdef Z0_DEBUG
	printf("%s\n", request->method);
	printf("%s\n", request->path);
#endif	

	i = 0;
	while (buff[i] != '\n')
		++i;	
	buff += i + 1;

	z0_list_init(&request->headers, sizeof (z0_hdr));

	z0_hdr header;

	do {
		i = 0;
		while (buff[i] != ':' && buff[i] != '\0')
			++i;

		if (buff[i] == '\0')
			break;

		buff[i] = '\0';
		header.key = z0_string_init(buff);
		buff += i + 2;
#ifdef Z0_DEBUG
		printf("%s: ", header.key);
#endif

		i = 0;
		while (buff[i] != '\n' && buff[i] != '\0')
			++i;
		
		if (buff[i] == '\0') {
			z0_string_free(header.key);
			break;
		}

		buff[i] = '\0';
		header.val = z0_string_init(buff);
		buff += i + 1;
#ifdef Z0_DEBUG
		printf("%s\n", header.val);
#endif
		z0_list_add(&request->headers, (void*)&header, sizeof (header));

		if (buff[0] == '\n' || buff[0] == '\r')
			break;
		
	} while (true);
}

void
z0_web_request_free(z0_req *request) {
	void *ptr;
	for (size_t i = 0; i < request->headers.len; ++i) {
		ptr = (uint8_t*)request->headers.data + i * sizeof (z0_hdr);	

		z0_string_free(((z0_hdr*)ptr)->key);
		z0_string_free(((z0_hdr*)ptr)->val);
	}
		

	z0_list_free(&request->headers);
	z0_string_free(request->path);
	z0_string_free(request->method);
}
